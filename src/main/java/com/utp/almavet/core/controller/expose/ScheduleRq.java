package com.utp.almavet.core.controller.expose;

import lombok.Data;

import java.util.List;

@Data
public class ScheduleRq {

    private Integer day;
    private List<Integer> hours;

}
