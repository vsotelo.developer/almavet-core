package com.utp.almavet.core.controller.expose;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class FCMMessageRq<D> {

    private Notification notification;
    private String priority;
    private String to;
    private D data;

}