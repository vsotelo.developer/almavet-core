package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SummaryRs {

    private String id;
    private String observation;

    private List<AttachedRs> attached;
    private List<PharmacyRs> pharmacy;
}
