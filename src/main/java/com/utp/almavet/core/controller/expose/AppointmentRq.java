package com.utp.almavet.core.controller.expose;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class AppointmentRq {

    private String name;
    private Boolean status;
    private Date startDate;
    private Date endDate;
    private String details;

    private PetRq pet;

    private AppointmentTypeRq appointmentType;
}
