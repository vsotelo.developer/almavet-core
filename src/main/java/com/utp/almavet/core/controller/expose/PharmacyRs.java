package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PharmacyRs {
    private String id;
    private String medicine;
    private String comment;
    private String dose;
}
