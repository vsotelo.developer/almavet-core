package com.utp.almavet.core.controller.expose;

import com.utp.almavet.core.arangodb.entity._Schedule;
import lombok.Data;

import java.util.List;

@Data
public class AppointmentTypeRq {

    private String id;
    private String name;

    private List<ScheduleRq> scheduleList;

}
