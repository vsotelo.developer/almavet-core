package com.utp.almavet.core.controller.expose;

import lombok.Data;

@Data
public class PharmacyRq {
    private String medicine;
    private String comment;
    private String dose;
}
