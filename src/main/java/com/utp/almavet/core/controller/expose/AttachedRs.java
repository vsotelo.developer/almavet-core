package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AttachedRs {
    private String id;
    private String urlAccess;
}
