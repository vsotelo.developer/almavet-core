package com.utp.almavet.core.controller.expose;


import lombok.Data;

@Data
public class ObjectResponse<T> {

    private String message;
    private T result;

}