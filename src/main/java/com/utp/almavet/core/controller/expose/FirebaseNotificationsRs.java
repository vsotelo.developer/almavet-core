package com.utp.almavet.core.controller.expose;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class FirebaseNotificationsRs {

    private String message;
}
