package com.utp.almavet.core.controller.expose;

import lombok.Data;

@Data
public class AttachedRq {

    private String urlAccess;
}
