package com.utp.almavet.core.controller.expose;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PersonRq {

    @NotEmpty(message = "required")
    private String name;

    @NotEmpty(message = "required")
    private String lastName;

    private String phoneNumber;

    @NotEmpty(message = "required")
    private String emailAddress;

    @NotEmpty(message = "required")
    private String documentNumber;
}
