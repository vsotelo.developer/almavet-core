package com.utp.almavet.core.controller;

import com.utp.almavet.core.controller.expose.*;
import com.utp.almavet.core.service.PersonService;
import com.utp.almavet.core.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/person")
public class PersonController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/user/register/")
    public ResponseEntity<ObjectResponse<PersonRs>> registerPerson(@RequestBody @Valid PersonRq personRq, ServletRequest servletRequest) throws Exception {
        LOGGER.info("registerAndSendToken() -> personRq : {}", personRq.getDocumentNumber());
        final String userId = (String) servletRequest.getAttribute("id");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                personService.registerPerson(personRq, userId)), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<ObjectResponse<PersonRs>> getPerson(ServletRequest servletRequest) throws Exception {
        LOGGER.info("getPerson()");
        final String userId = (String) servletRequest.getAttribute("id");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                personService.getPerson(userId)), HttpStatus.OK);
    }

}
