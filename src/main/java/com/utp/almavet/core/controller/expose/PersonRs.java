package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonRs {

    private String id;
    private String name;
    private String lastName;
    private String phoneNumber;
    private String emailAddress;
    private String documentNumber;
}
