package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class AppointmentRs {

    private String id;
    private String name;
    private Boolean status;
    private Date startDate;
    private Date endDate;
    private String details;

    //private SummaryRs summary;
    private PetRs pet;

    private AppointmentTypeRs appointmentType;

}
