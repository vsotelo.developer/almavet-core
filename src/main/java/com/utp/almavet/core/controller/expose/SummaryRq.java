package com.utp.almavet.core.controller.expose;

import lombok.Data;

import java.util.List;

@Data
public class SummaryRq {

    private String observation;

    private List<AttachedRq> attacheds;
    private List<PharmacyRq> pharmacys;
}
