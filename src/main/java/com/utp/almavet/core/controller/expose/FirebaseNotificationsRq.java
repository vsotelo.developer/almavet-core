package com.utp.almavet.core.controller.expose;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class FirebaseNotificationsRq {

    private String fcmToken;
    private String type;
    private String message;
}
