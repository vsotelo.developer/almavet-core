package com.utp.almavet.core.controller;

import com.utp.almavet.core.controller.expose.*;
import com.utp.almavet.core.service.PetService;
import com.utp.almavet.core.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pet")
public class PetController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PetController.class);

    private final PetService petService;

    @Autowired
    public PetController(PetService petService) {
        this.petService = petService;
    }

    @PostMapping("/pet/register/")
    public ResponseEntity<ObjectResponse<PetRs>> registerPet(@RequestBody @Valid PetRq petRq, ServletRequest servletRequest) throws Exception {
        LOGGER.info("registerPet() -> petRq : {}", petRq);
        final String userId = (String) servletRequest.getAttribute("id");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                petService.registerPet(petRq, userId)), HttpStatus.OK);
    }

    @DeleteMapping(value = "/pet/{petId}")
    public ResponseEntity<ObjectResponse<String>> deletePost(@PathVariable String petId) throws Exception {
        LOGGER.info("deletePet() -> petRq : {}", petId);
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                petService.deletePet(petId)), HttpStatus.OK);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<ObjectResponse<List<PetRs>>> getAll(ServletRequest servletRequest) throws Exception {
        LOGGER.info("getAll()");
        final String userId = (String) servletRequest.getAttribute("id");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                petService.getAll(userId)), HttpStatus.OK);
    }

}
