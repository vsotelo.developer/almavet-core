package com.utp.almavet.core.controller.expose;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UserRq {

    @NotEmpty(message = "required")
    private String username;

    @NotEmpty(message = "required")
    private String password;

    @NotEmpty(message = "required")
    private String tokenFCM;
}
