package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ScheduleRs {
    private String id;
    private Integer day;
    private List<Integer> hours;
}
