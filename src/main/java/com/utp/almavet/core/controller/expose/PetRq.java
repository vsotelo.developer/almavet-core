package com.utp.almavet.core.controller.expose;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data

public class PetRq {

    private String id;

    @NotEmpty(message = "required")
    private String name;

    private Integer age;
    private Integer weight;
    private String gender;
    private String breed;
    private String specie;
}
