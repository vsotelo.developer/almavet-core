package com.utp.almavet.core.controller;

import com.utp.almavet.core.controller.expose.*;
import com.utp.almavet.core.service.AppointmentTypeService;
import com.utp.almavet.core.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/appointmentType")
public class AppointmentTypeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentTypeController.class);

    private final AppointmentTypeService appointmentTypeService;

    @Autowired
    public AppointmentTypeController(AppointmentTypeService appointmentTypeService) {
        this.appointmentTypeService = appointmentTypeService;
    }


    @PostMapping("/register/")
    public ResponseEntity<ObjectResponse<AppointmentTypeRs>> registerAppointmentType(@RequestBody @Valid AppointmentTypeRq appointmentTypeRq) throws Exception {
        LOGGER.info("registerAppointmentType() -> AppointmentTypeRq : {}", appointmentTypeRq.toString());
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                appointmentTypeService.registerAppointmentType(appointmentTypeRq)), HttpStatus.OK);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<ObjectResponse<List<AppointmentTypeRs>>> getAll() throws Exception {
        LOGGER.info("getAll()");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                appointmentTypeService.getAll()), HttpStatus.OK);
    }



}
