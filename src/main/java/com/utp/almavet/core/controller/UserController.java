package com.utp.almavet.core.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.almavet.core.controller.expose.AuthResponse;
import com.utp.almavet.core.controller.expose.ObjectResponse;
import com.utp.almavet.core.controller.expose.UserRq;
import com.utp.almavet.core.service.UserService;
import com.utp.almavet.core.util.ResponseUtil;
import com.utp.almavet.core.util.ServiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user/register/")
    public ResponseEntity<ObjectResponse<AuthResponse>> registerAndSendToken(@RequestBody @Valid UserRq userRq) throws Exception {
        LOGGER.info("registerAndSendToken() -> userRq : {}", userRq);
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                userService.registerAndSendToken(userRq)), HttpStatus.OK);
    }
}
