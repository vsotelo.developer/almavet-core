package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PetRs {
    private String id;
    private String name;
    private Integer age;
    private Integer weight;
    private String gender;
    private String breed;
    private String specie;
}
