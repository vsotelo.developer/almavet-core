package com.utp.almavet.core.controller.expose;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthResponse {

    private boolean isNewUser;
    private String token;
}
