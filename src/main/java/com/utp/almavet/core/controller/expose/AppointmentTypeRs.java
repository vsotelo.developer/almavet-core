package com.utp.almavet.core.controller.expose;

import com.utp.almavet.core.arangodb.entity._Schedule;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AppointmentTypeRs {

    private String id;

    private String name;

    private List<ScheduleRs> scheduleList;
}
