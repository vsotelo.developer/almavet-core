package com.utp.almavet.core.controller;

import com.utp.almavet.core.controller.expose.AppointmentRq;
import com.utp.almavet.core.controller.expose.AppointmentRs;
import com.utp.almavet.core.controller.expose.ObjectResponse;
import com.utp.almavet.core.controller.expose.PetRs;
import com.utp.almavet.core.service.AppointmentService;
import com.utp.almavet.core.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentController.class);

    private final AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService mAppointmentService) {
        this.appointmentService = mAppointmentService;
    }

    @PostMapping("/register/")
    public ResponseEntity<ObjectResponse<AppointmentRs>> registerAppointment(@RequestBody @Valid AppointmentRq appointmentRq, ServletRequest servletRequest) throws Exception {
        LOGGER.info("registerAppointment() -> appointmentRq : {}", appointmentRq.toString());
        final String userId = (String) servletRequest.getAttribute("id");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                appointmentService.registerAppointment(appointmentRq, userId)), HttpStatus.OK);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<ObjectResponse<List<AppointmentRs>>> getAll(ServletRequest servletRequest) throws Exception {
        LOGGER.info("getAll()");
        final String userId = (String) servletRequest.getAttribute("id");
        return new ResponseEntity<>(ResponseUtil.createResponse("message.ok.operation.successfully",
                appointmentService.getAll(userId)), HttpStatus.OK);
    }


}
