package com.utp.almavet.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlmavetCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlmavetCoreApplication.class, args);
	}

}
