package com.utp.almavet.core.config;

import com.arangodb.ArangoDB;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.AbstractArangoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableArangoRepositories(basePackages = {"com.utp.almavet.core.arangodb.repository"})
public class ArangoDBConfig extends AbstractArangoConfiguration {

    @Override
    public ArangoDB.Builder arango() {
        return new ArangoDB.Builder()
                .host("docker-dev-1.pe1.linkfast.net.pe", 8529)
                .user("usr_uni")
                .password("usr_uni");
    }

    @Override
    public String database() {
        return "db_uni";
    }
}
//docker-dev-1.pe1.linkfast.net.pe:8529