package com.utp.almavet.core.config;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandlerResolver {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<CustomResponseException> customException(Exception e, ServletWebRequest servletWebRequest) {
        e.printStackTrace();
        CustomResponseException responseException =
                CustomResponseException.builder()
                        .timestamp(LocalDateTime.now())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .error(e.getMessage())
                        .path(servletWebRequest.getRequest().getRequestURI())
                        .build();
        return new ResponseEntity<>(responseException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<CustomResponseException> invalidInputRequest(MethodArgumentNotValidException exception, ServletWebRequest servletWebRequest) {
        exception.printStackTrace();
        String messagesErrorJoiner = exception.getBindingResult().getFieldErrors()
                .stream().map(fieldError -> StringUtils.join(fieldError.getField(), " : ", fieldError.getDefaultMessage()))
                .collect(Collectors.joining(", "));
        CustomResponseException responseException =
                CustomResponseException.builder()
                        .timestamp(LocalDateTime.now())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .error(messagesErrorJoiner)
                        .path(servletWebRequest.getRequest().getRequestURI())
                        .build();
        return new ResponseEntity<>(responseException, HttpStatus.BAD_REQUEST);
    }

    @Builder
    @Data
    private static class CustomResponseException {

        private LocalDateTime timestamp;
        private int status;
        private String error;
        private String path;

    }
}
