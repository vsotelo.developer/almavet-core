package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._AppointmentType;

public interface AppointmentTypeRepository extends ArangoRepository<_AppointmentType, String> {


}
