package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._Attached;

public interface AttachedRepository extends ArangoRepository<_Attached, String> {


}
