package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._User;

import java.util.Optional;

public interface UserRepository extends ArangoRepository<_User, String> {

    Optional<_User> findByUsername(String username);
}
