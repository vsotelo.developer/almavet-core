package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;

import com.utp.almavet.core.arangodb.entity._Pet;

public interface PetRepository extends ArangoRepository<_Pet, String> {


}
