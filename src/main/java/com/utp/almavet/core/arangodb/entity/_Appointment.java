package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Ref;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Builder
@Document("appointment")
public class _Appointment {

    // ID
    @Id private String id;

    // Attributes
    private String name;
    private Boolean status;
    private Date startDate;
    private Date endDate;
    private String details;

    // Relations
    @Ref
    private _Summary summary;

    @Ref
    private _Pet pet;

    @Ref
    private _AppointmentType appointmentType;

    // Logs
    private Date insertAt;
    private Date updateAt;
}
