package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Ref;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document("summary")
public class _Summary {

    // ID
    @Id private String id;

    // Attributes
    private String observation;

    @Ref
    private _Appointment appointment;

    @Ref
    private List<_Attached> attacheds;

    @Ref
    private List<_Pharmacy> pharmacies;

    // Logs
    private Date insertAt;
    private Date updateAt;
}
