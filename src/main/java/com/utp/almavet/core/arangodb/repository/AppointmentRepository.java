package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._Appointment;

public interface AppointmentRepository extends ArangoRepository<_Appointment, String> {


}
