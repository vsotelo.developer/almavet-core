package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Ref;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document("appointmentType")
public class _AppointmentType {

    // ID
    @Id private String id;

    // Attributes
    private String name;

    @Ref
    private List<_Schedule> schedules;

    // Logs
    private Date insertAt;
    private Date updateAt;
}
