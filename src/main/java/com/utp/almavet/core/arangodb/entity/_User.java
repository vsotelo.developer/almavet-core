package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.PersistentIndexed;
import com.arangodb.springframework.annotation.Ref;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document("user")
public class _User {

    // ID
    @Id private String id;

    // Attributes
    @PersistentIndexed(unique = true)
    private String username;
    private String password;
    private Boolean status;
    private String tokenFCM;

    // Relations
    @Ref private _Person person;

    @Ref private List<_Appointment> appointments;

    // Logs
    private Date insertAt;
    private Date updateAt;

}
