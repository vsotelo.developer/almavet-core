package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Ref;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Builder
@Document("pet")
public class _Pet {

    // ID
    @Id private String id;

    // Attributes
    private String name;
    private Integer age;
    private Integer weight;
    private String gender;
    private String breed;
    private String specie;

    private Boolean active;

    // Logs
    private Date insertAt;
    private Date updateAt;
}
