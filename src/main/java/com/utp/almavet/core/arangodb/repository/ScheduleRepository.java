package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._Schedule;

public interface ScheduleRepository extends ArangoRepository<_Schedule, String> {


}
