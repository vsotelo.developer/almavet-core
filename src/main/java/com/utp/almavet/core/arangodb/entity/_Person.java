package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Ref;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document("person")
public class _Person {

    // ID
    @Id private String id;

    // Attributes
    private String name;
    private String lastName;
    private String phoneNumber;
    private String emailAddress;
    private String documentNumber;

    // Relations
    @Ref private List<_Pet> pets;

    // Logs
    private Date insertAt;
    private Date updateAt;
}
