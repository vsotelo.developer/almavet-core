package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._Summary;

public interface SummaryRepository extends ArangoRepository<_Summary, String> {


}
