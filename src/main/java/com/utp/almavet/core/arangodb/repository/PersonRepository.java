package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._Person;

public interface PersonRepository extends ArangoRepository<_Person, String> {
}
