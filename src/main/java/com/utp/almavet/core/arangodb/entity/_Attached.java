package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Ref;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Builder
@Document("attached")
public class _Attached {

    // ID
    @Id private String id;

    // Attributes
    private String urlAccess;

    @Ref
    private _Summary summary;


    // Logs
    private Date insertAt;
    private Date updateAt;
}
