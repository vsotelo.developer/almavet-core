package com.utp.almavet.core.arangodb.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.utp.almavet.core.arangodb.entity._Pharmacy;

public interface PharmacyRepository extends ArangoRepository<_Pharmacy, String> {


}
