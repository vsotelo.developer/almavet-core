package com.utp.almavet.core.arangodb.entity;

import com.arangodb.springframework.annotation.Document;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document("schedule")
public class _Schedule {

    // ID
    @Id private String id;

    // Attributes
    private Integer day;
    private List<Integer> hours;

    // Logs
    private Date insertAt;
    private Date updateAt;
}
