package com.utp.almavet.core.util;

import com.utp.almavet.core.controller.expose.ObjectResponse;

public class ResponseUtil {

    public static <R> ObjectResponse<R> createResponse(String message, R result) {
        ObjectResponse<R> _result = new ObjectResponse<>();
        _result.setMessage(message);
        _result.setResult(result);
        return _result;
    }
}
