package com.utp.almavet.core.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static Calendar dateToCalendar(Date myDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        return cal;
    }
}
