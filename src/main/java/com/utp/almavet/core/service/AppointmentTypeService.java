package com.utp.almavet.core.service;

import com.utp.almavet.core.controller.expose.AppointmentTypeRq;
import com.utp.almavet.core.controller.expose.AppointmentTypeRs;
import com.utp.almavet.core.controller.expose.PetRs;

import java.util.List;

public interface AppointmentTypeService {

    AppointmentTypeRs registerAppointmentType(AppointmentTypeRq request) throws Exception;

    List<AppointmentTypeRs> getAll() throws Exception;
}
