package com.utp.almavet.core.service;

import com.utp.almavet.core.controller.expose.PersonRq;
import com.utp.almavet.core.controller.expose.PersonRs;

public interface PersonService {

    PersonRs registerPerson(PersonRq request, String userId) throws Exception;

    PersonRs getPerson(String userId) throws Exception;
}
