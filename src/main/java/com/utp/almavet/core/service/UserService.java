package com.utp.almavet.core.service;

import com.utp.almavet.core.controller.expose.AuthResponse;
import com.utp.almavet.core.controller.expose.UserRq;

public interface UserService {

    AuthResponse registerAndSendToken(UserRq userRq) throws Exception;
}
