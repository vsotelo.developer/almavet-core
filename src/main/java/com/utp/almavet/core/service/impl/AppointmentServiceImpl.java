package com.utp.almavet.core.service.impl;

import com.utp.almavet.core.arangodb.entity.*;
import com.utp.almavet.core.arangodb.repository.*;
import com.utp.almavet.core.controller.expose.*;
import com.utp.almavet.core.service.AppointmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentServiceImpl.class);

    private final AppointmentRepository appointmentRepository;
    private final AppointmentTypeRepository appointmentTypeRepository;
    private final PetRepository petRepository;
    private final UserRepository userRepository;

    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, AppointmentTypeRepository appointmentTypeRepository, PetRepository petRepository, UserRepository userRepository) {
        this.appointmentRepository = appointmentRepository;
        this.appointmentTypeRepository = appointmentTypeRepository;
        this.petRepository = petRepository;
        this.userRepository = userRepository;
    }

    @Override
    public AppointmentRs registerAppointment(AppointmentRq request, String userId) throws Exception {
        LOGGER.info("registerAppointment() -> userId : {}", userId);
        try {

            _Appointment._AppointmentBuilder appointment = _Appointment.builder()
                    .name(request.getName())
                    .status(true)
                    .startDate(request.getStartDate())
                    .endDate(request.getEndDate())
                    .details(request.getDetails())
                    .insertAt(new Date());



            // valida objeto Summary diferente a null


            if(!Objects.isNull(request.getAppointmentType())) {

                if (request.getAppointmentType().getId() == null) {
                    throw new Exception("Appointment Type id is required");
                }
                Optional<_AppointmentType> optionalAppointmentType = appointmentTypeRepository.findById(request.getAppointmentType().getId());

                if (optionalAppointmentType.isEmpty()){
                    throw new Exception("Appointment Type not exist");
                }
                _AppointmentType appointmentType = optionalAppointmentType.get();
                appointment.appointmentType(
                        _AppointmentType.builder()
                                .id(appointmentType.getId())
                                .name(appointmentType.getName())
                                .schedules(appointmentType.getSchedules())
                        .build());
            }

            if(!Objects.isNull(request.getPet())) {

                if (request.getPet().getId() == null) {
                    throw new Exception("Pet id is required");
                }
                Optional<_Pet> optionalPet = petRepository.findById(request.getPet().getId());

                if (optionalPet.isEmpty()){
                    throw new Exception("Pet exist");
                }
                _Pet pet = optionalPet.get();
                appointment.pet(
                        _Pet.builder()
                                .id(pet.getId())
                                .name(pet.getName())
                                .age(pet.getAge())
                                .weight(pet.getWeight())
                                .gender(pet.getGender())
                                .breed(pet.getBreed())
                                .specie(pet.getSpecie())
                                .active(true)
                                .build());
            }
            _Appointment appointmentSaved = appointment.build();
            appointmentRepository.save(appointmentSaved);

            Optional<_User> optionalUser = userRepository.findById(userId);

            if (optionalUser.isEmpty()) {
                throw new Exception("User not exist");
            }

            _User user = optionalUser.get();

            if (CollectionUtils.isEmpty(user.getAppointments())) {
                user.setAppointments(new ArrayList<>());
            }

            user.setUpdateAt(new Date());
            user.getAppointments().add(appointmentSaved);
            userRepository.save(user);

            return AppointmentRs.builder()
                    .id(appointmentSaved.getId())
                    .name(appointmentSaved.getName())
                    .status(appointmentSaved.getStatus())
                    .startDate(appointmentSaved.getStartDate())
                    .endDate(appointmentSaved.getEndDate())
                    .details(appointmentSaved.getDetails())
                    .appointmentType(AppointmentTypeRs.builder()
                            .id(appointmentSaved.getAppointmentType().getId())
                            .name(appointmentSaved.getAppointmentType().getName())
                            .scheduleList(appointmentSaved.getAppointmentType().getSchedules().stream()
                                    .map(schedule -> ScheduleRs.builder()
                                            .id(schedule.getId())
                                            .day(schedule.getDay())
                                            .hours(schedule.getHours())
                                            .build()

                            ).collect(Collectors.toList()))
                            .build())
                    .pet(PetRs.builder()
                            .id(appointmentSaved.getPet().getId())
                            .name(appointmentSaved.getPet().getName())
                            .age(appointmentSaved.getPet().getAge())
                            .weight(appointmentSaved.getPet().getWeight())
                            .gender(appointmentSaved.getPet().getGender())
                            .breed(appointmentSaved.getPet().getBreed())
                            .specie(appointmentSaved.getPet().getSpecie())
                            .build())
                    /*.summary(SummaryRs.builder()
                            .id(appointmentSaved.getSummary().getId())
                            .observation(appointmentSaved.getSummary().getObservation())
                            .pharmacy(appointmentSaved.getSummary().getPharmacies().stream()
                                    .map(pharmacy -> PharmacyRs.builder()
                                            .id(pharmacy.getId())
                                            .dose(pharmacy.getDose())
                                            .medicine(pharmacy.getMedicine())
                                            .comment(pharmacy.getComment())
                                            .build()).collect(Collectors.toList()))
                            .attached(appointmentSaved.getSummary().getAttacheds().stream()
                                    .map(attached -> AttachedRs.builder()
                                            .id(attached.getId())
                                            .urlAccess(attached.getUrlAccess())
                                            .build()).collect(Collectors.toList()))
                            .build())*/
                    .build();


        } catch (Exception e) {
            LOGGER.error("[ERROR] registerAppointment Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.register.appointment");
        }
    }

    @Override
    public List<AppointmentRs> getAll(String userId) throws Exception {
        try {
            Optional<_User> optionalUser = userRepository.findById(userId);

            if (optionalUser.isEmpty()) {
                throw new Exception("User not exist");
            }

            _User user = optionalUser.get();

            if (!CollectionUtils.isEmpty(user.getAppointments())) {
                return user.getAppointments().stream()
                        .map(appointment -> AppointmentRs.builder()
                                .id(appointment.getId())
                                .name(appointment.getName())
                                .status(appointment.getStatus())
                                .startDate(appointment.getStartDate())
                                .endDate(appointment.getEndDate())
                                .details(appointment.getDetails())
                                //appointmentType
                                .appointmentType(AppointmentTypeRs
                                        .builder()
                                        .id(appointment.getAppointmentType().getId())
                                        .name(appointment.getAppointmentType().getName())
                                        .scheduleList(appointment.getAppointmentType().getSchedules().stream()
                                                .map(schedule -> ScheduleRs.builder()
                                                        .id(schedule.getId())
                                                        .day(schedule.getDay())
                                                        .hours(schedule.getHours())
                                                        .build()

                                                ).collect(Collectors.toList()))
                                        .build())
                                /*summary
                                .summary(appointment.getSummary() != null ? SummaryRs.builder()
                                        .id(appointment.getSummary().getId())
                                        .observation(appointment.getSummary().getObservation())
                                        //pharmacy
                                        .pharmacy(appointment.getSummary().getPharmacies().stream()
                                                .map(pharmacy -> PharmacyRs.builder()
                                                        .id(pharmacy.getId())
                                                        .dose(pharmacy.getDose())
                                                        .medicine(pharmacy.getMedicine())
                                                        .comment(pharmacy.getComment())
                                                        .build()).collect(Collectors.toList()))
                                        //attached
                                        .attached(appointment.getSummary().getAttacheds().stream()
                                                .map(attached -> AttachedRs.builder()
                                                        .id(attached.getId())
                                                        .urlAccess(attached.getUrlAccess())
                                                        .build()).collect(Collectors.toList()))
                                        .build() : null)*/
                                .build()).collect(Collectors.toList());
            }

            return Collections.emptyList();
        } catch (Exception e) {
            LOGGER.error("[ERROR] getAll() Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.getall.appointment");
        }
    }
}

/*
_Summary._SummaryBuilder summaryBuilder = _Summary.builder();
if(!Objects.isNull(request.getSummary())){

                // valida lista de Pharmacys en Summary diferente a null
                if (!CollectionUtils.isEmpty(request.getSummary().getPharmacys())) {
                    List<_Pharmacy> pharmacyList = new ArrayList<>();
                    request.getSummary().getPharmacys().forEach(pharmacyRq -> {
                        _Pharmacy pharmacy = _Pharmacy.builder()
                                .medicine(pharmacyRq.getMedicine())
                                .comment(pharmacyRq.getComment())
                                .dose(pharmacyRq.getDose())
                                .summary(summaryBuilder.build())
                                .insertAt(new Date())
                                .build();
                        pharmacyRepository.save(pharmacy);
                        pharmacyList.add(pharmacy);
                    });
                    summaryBuilder.pharmacies(pharmacyList);
                }

                // valida lista de attacheds en Summary diferente a null
                if (!CollectionUtils.isEmpty(request.getSummary().getAttacheds())) {
                    List<_Attached> attachedList = new ArrayList<>();
                    request.getSummary().getAttacheds().forEach(attachedRq -> {
                        _Attached attached = _Attached.builder()
                                .urlAccess(attachedRq.getUrlAccess())
                                .summary(summaryBuilder.build())
                                .insertAt(new Date())
                                .build();
                        attachedRepository.save(attached);
                        attachedList.add(attached);

                    });
                    summaryBuilder.attacheds(attachedList);
                }

                summaryBuilder.observation(request.getSummary().getObservation());
                summaryBuilder.insertAt(new Date());
                _Summary summarySaved = summaryBuilder.build();
                summaryRepository.save(summarySaved);

                appointment.summary(summarySaved);

            }
*/