package com.utp.almavet.core.service.impl;

import com.utp.almavet.core.arangodb.entity.*;
import com.utp.almavet.core.arangodb.repository.*;
import com.utp.almavet.core.controller.expose.*;
import com.utp.almavet.core.service.AppointmentService;
import com.utp.almavet.core.service.AppointmentTypeService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AppointmentTypeServiceImpl implements AppointmentTypeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentTypeServiceImpl.class);

    private final AppointmentTypeRepository appointmentTypeRepository;

    private final ScheduleRepository scheduleRepository;

    @Autowired
    public AppointmentTypeServiceImpl(AppointmentTypeRepository appointmentTypeRepository, ScheduleRepository scheduleRepository) {
        this.appointmentTypeRepository = appointmentTypeRepository;
        this.scheduleRepository = scheduleRepository;
    }


    @Override
    public AppointmentTypeRs registerAppointmentType(AppointmentTypeRq request) throws Exception {

        try {
            List<_Schedule> listScheduleToSave = new ArrayList<>();
            request.getScheduleList().forEach(schedule -> {
                _Schedule scheduleToSave = _Schedule.builder()
                        .day(schedule.getDay())
                        .hours(schedule.getHours())
                        .insertAt(new Date())
                        .build();
                scheduleRepository.save(scheduleToSave);
                listScheduleToSave.add(scheduleToSave);
            });

            _AppointmentType apointAppointmentType = _AppointmentType.builder()
                    .name(request.getName())
                    .schedules(listScheduleToSave)
                    .insertAt(new Date())
                    .build();


            appointmentTypeRepository.save(apointAppointmentType);

            return AppointmentTypeRs.builder()
                    .id(apointAppointmentType.getId())
                    .name(apointAppointmentType.getName())
                    .scheduleList(apointAppointmentType.getSchedules().stream().map(schedule ->
                            ScheduleRs.builder()
                                    .id(schedule.getId())
                                    .day(schedule.getDay())
                                    .hours(schedule.getHours())
                                    .build()
                    ).collect(Collectors.toList()))
                    .build();

        } catch (Exception e) {
            LOGGER.error("[ERROR] registerAppointment Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.register.appointment");
        }
    }

    @Override
    public List<AppointmentTypeRs> getAll() throws Exception {
        try {
            Iterable<_AppointmentType> optionalAppointmentType = appointmentTypeRepository.findAll();

            List<AppointmentTypeRs> listToReturn = new ArrayList<>();
            optionalAppointmentType.iterator().forEachRemaining(appointmentType -> {
                listToReturn.add(AppointmentTypeRs
                        .builder()
                        .id(appointmentType.getId())
                        .name(appointmentType.getName())
                        .scheduleList(appointmentType.getSchedules().stream().map(schedule ->
                                ScheduleRs.builder()
                                        .id(schedule.getId())
                                        .day(schedule.getDay())
                                        .hours(schedule.getHours())
                                        .build())
                                .collect(Collectors.toList()))
                        .build());
            });


            return listToReturn;
        } catch (Exception e) {
            LOGGER.error("[ERROR] getAll() Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.getall.pet");
        }
    }
}
