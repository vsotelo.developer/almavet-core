package com.utp.almavet.core.service;


import com.utp.almavet.core.controller.expose.PetRq;
import com.utp.almavet.core.controller.expose.PetRs;

import java.util.List;

public interface PetService {
    PetRs registerPet(PetRq request, String userId) throws Exception;

    String deletePet(String petId) throws Exception;

    List<PetRs> getAll(String userId) throws Exception;
}
