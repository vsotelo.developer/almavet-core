package com.utp.almavet.core.service.impl;


import com.utp.almavet.core.arangodb.entity._Person;
import com.utp.almavet.core.arangodb.entity._Pet;
import com.utp.almavet.core.arangodb.entity._User;
import com.utp.almavet.core.arangodb.repository.PersonRepository;
import com.utp.almavet.core.arangodb.repository.PetRepository;
import com.utp.almavet.core.arangodb.repository.UserRepository;
import com.utp.almavet.core.controller.expose.PetRq;
import com.utp.almavet.core.controller.expose.PetRs;
import com.utp.almavet.core.service.PetService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class PetServiceImpl implements PetService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PetService.class);

    private final PetRepository petRepository;
    private final UserRepository userRepository;
    private final PersonRepository personRepository;

    @Autowired
    public PetServiceImpl(PetRepository petRepository, UserRepository userRepository, PersonRepository personRepository) {
        this.petRepository = petRepository;
        this.userRepository = userRepository;
        this.personRepository = personRepository;
    }


    @Override
    public PetRs registerPet(PetRq request, String userId) throws Exception {
        LOGGER.info("user() -> userId : {}", userId);

        try {

            Optional<_User> optionalUser = userRepository.findById(userId);

            if (optionalUser.isEmpty()) {
                throw new Exception("User not exist");
            }
            _User user = optionalUser.get();
            if (user.getPerson() == null) {
                throw new Exception("Person not exist");
            }

            _Person person = user.getPerson();

            if (CollectionUtils.isEmpty(person.getPets())) {
                person.setPets(new ArrayList<>());
            }

            _Pet pet = _Pet.builder()
                    .name(request.getName())
                    .age(request.getAge())
                    .weight(request.getWeight())
                    .gender(request.getGender())
                    .breed(request.getBreed())
                    .specie(request.getSpecie())
                    .active(true)
                    .insertAt(new Date())
                    .build();

            petRepository.save(pet);
            person.getPets().add(pet);

            personRepository.save(person);
            return PetRs.builder()
                    .id(pet.getId())
                    .name(pet.getName())
                    .age(pet.getAge())
                    .weight(pet.getWeight())
                    .gender(pet.getGender())
                    .breed(pet.getBreed())
                    .specie(pet.getSpecie())
                    .build();

        } catch (Exception e) {
            LOGGER.error("[ERROR] registerPet Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.register.pet");
        }
    }

    @Override
    public String deletePet(String petId) throws Exception {
        try {

            Optional<_Pet> optionalPet = petRepository.findById(petId);
            if (optionalPet.isPresent()) {
                _Pet pet = optionalPet.get();
                pet.setActive(false);
                petRepository.save(pet);
            }

            return petId;
        } catch (Exception e) {
            LOGGER.error("[ERROR] deletePet Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.delete.pet");
        }
    }

    @Override
    public List<PetRs> getAll(String userId) throws Exception {
        try {
            Optional<_User> optionalUser = userRepository.findById(userId);

            if (optionalUser.isEmpty()) {
                throw new Exception("User not exist");
            }

            _User user = optionalUser.get();
            if (user.getPerson() != null && CollectionUtils.isNotEmpty(user.getPerson().getPets())) {
                return user.getPerson().getPets().stream()
                        .filter(_Pet::getActive)
                        .map(pet -> PetRs.builder()
                                .id(pet.getId())
                                .name(pet.getName())
                                .age(pet.getAge())
                                .weight(pet.getWeight())
                                .gender(pet.getGender())
                                .breed(pet.getBreed())
                                .specie(pet.getSpecie())
                                .build()).collect(Collectors.toList());
            }

            return Collections.emptyList();
        } catch (Exception e) {
            LOGGER.error("[ERROR] getAll() Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.getall.pet");
        }
    }

}
