package com.utp.almavet.core.service.impl;

import com.utp.almavet.core.controller.expose.*;
import com.utp.almavet.core.exception.FCMException;
import com.utp.almavet.core.service.FCMService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class FCMServiceImpl implements FCMService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FCMServiceImpl.class);

    @Value("${fcm.server-url:null}")
    private String FCM_SERVER_URL;

    @Value("${fcm.server-token:null}")
    private String FCM_SERVER_TOKEN;

    private final RestTemplate restTemplate;

    @Autowired
    public FCMServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public FirebaseNotificationsRs sendNotification(FirebaseNotificationsRq request) throws Exception {
        LOGGER.debug("sendFCMNotifications() -> fcmToken: {}", request.getFcmToken());

        try {

            FCMMessageRq<RideMessage> fcmMessageRq = FCMMessageRq.<RideMessage>builder()
                    .notification(
                            Notification.builder()
                                    .title("Almavet Notification")
                                    .body(request.getMessage())
                                    .build()
                    )
                    .priority("high")
                    .data(
                            RideMessage.builder()
                                    .notification_type(request.getType())
                                    .build()
                    )
                    .to(request.getFcmToken())
                    .build();

            FCMMessageRs fcmMessageRs = send(fcmMessageRq);

            return FirebaseNotificationsRs.builder()
                    .message(fcmMessageRs != null && CollectionUtils.isNotEmpty(fcmMessageRs.getResults())
                            ? fcmMessageRs.getResults().get(NumberUtils.INTEGER_ZERO).getMessage_id() : StringUtils.EMPTY)
                    .build();

        } catch (FCMException e) {
            LOGGER.error("[ERROR] WiSeRPCResponseException: {}", e.getMessage(), e);
            throw new FCMException(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("[ERROR] Exception: {}", e.getMessage(), e);
            throw new Exception("Server Error to send Notification");
        }
    }



    public <D> FCMMessageRs send(FCMMessageRq<D> fcmMessageRq) throws FCMException {
        LOGGER.debug("sendFCMNotifications() -> fcmToken: {}", fcmMessageRq.getTo());

        try {

            // Creando peticion REST Para fcm.googleapis.com/fcm/send
            HttpHeaders headersRequest = new HttpHeaders();
            headersRequest.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headersRequest.add(HttpHeaders.AUTHORIZATION, FCM_SERVER_TOKEN);
            HttpEntity<FCMMessageRq<D>> fcmMessageHttpEntity = new HttpEntity<>(fcmMessageRq, headersRequest);

            // Envia Peticion
            ResponseEntity<FCMMessageRs> response = restTemplate.exchange(FCM_SERVER_URL, HttpMethod.POST, fcmMessageHttpEntity, FCMMessageRs.class);

            // Valida Respuesta
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            } else {
                throw new FCMException("Error to send notification");
            }

        } catch (FCMException e) {
            LOGGER.error("[ERROR] FCMException: {}", e.getMessage(), e);
            throw new FCMException(e.getMessage());
        } catch (Exception e) {
            LOGGER.error("[ERROR] Exception: {}", e.getMessage(), e);
            throw new FCMException("Server Error to send Notification");
        }

    }

}
