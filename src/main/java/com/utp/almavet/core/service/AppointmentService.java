package com.utp.almavet.core.service;

import com.utp.almavet.core.controller.expose.AppointmentRq;
import com.utp.almavet.core.controller.expose.AppointmentRs;

import java.util.List;

public interface AppointmentService {

    AppointmentRs registerAppointment(AppointmentRq request, String userId) throws Exception;

    List<AppointmentRs> getAll(String userId) throws Exception;
}
