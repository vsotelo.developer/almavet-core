package com.utp.almavet.core.service.impl;

import com.utp.almavet.core.arangodb.entity._Person;
import com.utp.almavet.core.arangodb.entity._User;
import com.utp.almavet.core.arangodb.repository.PersonRepository;
import com.utp.almavet.core.arangodb.repository.UserRepository;
import com.utp.almavet.core.controller.expose.PersonRq;
import com.utp.almavet.core.controller.expose.PersonRs;
import com.utp.almavet.core.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {


    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    private final PersonRepository personRepository;
    private final UserRepository userRepository;


    @Autowired
    public PersonServiceImpl(PersonRepository personRepository, UserRepository userRepository) {
        this.personRepository = personRepository;
        this.userRepository = userRepository;
    }

    @Override
    public PersonRs registerPerson(PersonRq request, String userId) throws Exception {
        LOGGER.info("registerPerson() -> userId : {}", userId);

        try {

            Optional<_User> optionalUser = userRepository.findById(userId);

            if (optionalUser.isEmpty()) {
                throw new Exception("User not exist");
            }

            _Person person = _Person.builder()
                    .name(request.getName())
                    .lastName(request.getLastName())
                    .phoneNumber(request.getPhoneNumber())
                    .emailAddress(request.getEmailAddress())
                    .documentNumber(request.getDocumentNumber())
                    .insertAt(new Date())
                    .build();

            personRepository.save(person);

            _User user = optionalUser.get();

            user.setPerson(person);
            user.setUpdateAt(new Date());
            userRepository.save(user);

            return PersonRs.builder()
                    .id(person.getId())
                    .name(person.getName())
                    .lastName(person.getLastName())
                    .phoneNumber(person.getPhoneNumber())
                    .emailAddress(person.getEmailAddress())
                    .documentNumber(person.getDocumentNumber())
                    .build();

        } catch (Exception e) {
            LOGGER.error("[ERROR] registerPerson Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.register.person");
        }
    }

    @Override
    public PersonRs getPerson(String userId) throws Exception {
        try {
            Optional<_User> optionalUser = userRepository.findById(userId);

            if (optionalUser.isEmpty()) {
                throw new Exception("User not exist");
            }
            _User user = optionalUser.get();

            if (user.getPerson() == null) {
                throw new Exception("Person not exist");
            }

            _Person person = user.getPerson();
            return PersonRs.builder()
                    .id(person.getId())
                    .name(person.getName())
                    .lastName(person.getLastName())
                    .phoneNumber(person.getPhoneNumber())
                    .emailAddress(person.getEmailAddress())
                    .documentNumber(person.getDocumentNumber())
                    .build();

        } catch (Exception e) {
            LOGGER.error("[ERROR] getPerson Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.get.person");
        }
    }
}
