package com.utp.almavet.core.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utp.almavet.core.arangodb.entity._User;
import com.utp.almavet.core.arangodb.repository.UserRepository;
import com.utp.almavet.core.controller.expose.AuthResponse;
import com.utp.almavet.core.controller.expose.UserRq;
import com.utp.almavet.core.service.UserService;
import com.utp.almavet.core.util.ServiceConstants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final Gson GSON = new GsonBuilder().serializeNulls().setDateFormat(ServiceConstants.LOGGER_FORMAT_DATE).create();

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public AuthResponse registerAndSendToken(UserRq userRq) throws Exception {
        LOGGER.info("registerAndSendToken() -> username : {}", userRq.getUsername());

        try {

            boolean isNew = false;

            Optional<_User> optionalUser = userRepository.findByUsername(userRq.getUsername());

            String token;
            _User user;

            // TODO: Cifrar pass
            if (optionalUser.isPresent()) { // validamos la contraseña
                user = optionalUser.get();
                if (!user.getPassword().equals(userRq.getPassword())) {
                    throw new Exception("La contraseña enviada no coincide con la del usuario.");
                }

            } else {
                isNew = true;
                user = _User.builder()
                        .username(userRq.getUsername())
                        // TODO: Cifrar pass
                        .password(userRq.getPassword())
                        .tokenFCM(userRq.getTokenFCM())
                        .status(true)
                        .insertAt(new Date())
                        .build();

                userRepository.save(user);

            }
            Map<String, Object> claims = new HashMap<>();

            claims.put("id", user.getId());
            claims.put("username", user.getUsername());

            Date now = new Date();
            Date expiration = new Date(now.getTime() + 31536000000L);

            token = Jwts.builder()
                    .setSubject(userRq.getUsername())
                    .setClaims(claims)
                    .setIssuedAt(now)
                    .setExpiration(expiration)
                    .signWith(SignatureAlgorithm.HS512, ServiceConstants.SECRET_KEY)
                    .compact();
            return AuthResponse.builder()
                    .token(token)
                    .isNewUser(isNew)
                    .build();

        } catch (Exception e) {
            LOGGER.error("[ERROR] registerAndSendToken Exception: {}", e.getMessage(), e);
            throw new Exception("error.service.auth");
        }
    }
}
