package com.utp.almavet.core.service;

import com.utp.almavet.core.controller.expose.FCMMessageRq;
import com.utp.almavet.core.controller.expose.FCMMessageRs;
import com.utp.almavet.core.controller.expose.FirebaseNotificationsRq;
import com.utp.almavet.core.controller.expose.FirebaseNotificationsRs;
import com.utp.almavet.core.exception.FCMException;

public interface FCMService {

    <D> FirebaseNotificationsRs sendNotification(FirebaseNotificationsRq fcmMessageRq) throws Exception;

}