package com.utp.almavet.core.exception;

public class FCMException extends Exception {

    public FCMException(String message) {
        super(message);
    }
}
